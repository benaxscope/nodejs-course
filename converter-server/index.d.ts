import { PassportUser } from '@/types/user';

export {};

declare global {
  namespace Express {
    interface Request {
      clientId?: string;
    }
    interface User extends PassportUser {}
  }
}
