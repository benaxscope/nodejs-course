import './env.setup';
import express from 'express';
import morgan from 'morgan';
import { passport } from './auth/passport.setup';
import session from 'express-session';

import { routes } from '@/routes';
import { runFileProcessingTaskPool } from '@/file-processing-task-pool';
const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: process.env.SESSION_SECRET_KEY!,
    saveUninitialized: true,
  })
);
app.use(morgan('dev'));

app.use(routes);

export const fileProcessingTaskPool = runFileProcessingTaskPool();

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
