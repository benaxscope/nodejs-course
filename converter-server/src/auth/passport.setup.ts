import jwt from 'jsonwebtoken';
import passport, { use } from 'passport';

import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as BearerStrategy } from 'passport-http-bearer';
import { PassportUser } from '../types/user';
import { getUser } from '@/services/user/get-user';
import { findUserByCredentials } from '@/services/user/find-user-by-credentials';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import { createUser } from '@/services/user/create-user';
import { randomUUID } from 'crypto';

passport.use(
  new LocalStrategy(async (username, password, cb) => {
    try {
      const user = await findUserByCredentials({ username, password });

      return cb(null, user);
    } catch {
      return cb(null, false);
    }
  })
);

passport.use(
  new BearerStrategy((token, cb) => {
    jwt.verify(
      token,
      process.env.PASSPORT_SECRET_KEY!,
      async (err, decoded) => {
        if (err) return cb(err);
        const decodedId = (decoded as any as PassportUser)?.id as
          | string
          | undefined;

        let user = undefined;
        if (decodedId != undefined) {
          user = await getUser({ id: decodedId });
        }

        return cb(null, user ? user : false);
      }
    );
  })
);

passport.serializeUser((user, done) => {
  done(null, (user as PassportUser)?.id);
});

passport.deserializeUser(async (id: string, done) => {
  const user = await getUser({ id });

  done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID!,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET!,
      callbackURL: process.env.GOOGLE_CALLBACK!,
    },
    async (token, tokenSecret, profile, cb) => {
      let user;
      try {
        user = await getUser({
          provider: { name: profile.provider, id: profile.id },
        });
      } catch {
        user = await createUser({
          username: profile.displayName!,
          password: randomUUID(),
          provider: {
            name: profile.provider,
            id: profile.id,
          },
        });
      }

      console.log(user);

      return cb(null, user);
    }
  )
);

export { passport };
