import multer from 'multer';
import path from 'path';
import fs from 'fs';
import { randomUUID } from 'node:crypto';

export const UPLOAD_FILE_PATH = path.resolve(__dirname, '../..', 'uploads');

export const storageConfig = multer.diskStorage({
  destination: (req, file, callback) => {
    const clientId = req.user?.id ?? req.sessionID;
    const destPath = path.resolve(UPLOAD_FILE_PATH, clientId);

    const dirExists = fs.existsSync(destPath);
    if (!dirExists) {
      fs.mkdirSync(destPath);
    }

    callback(null, destPath);
  },
  filename: function (req, file, cb) {
    const fileUUID = randomUUID();
    cb(null, `${fileUUID}${path.extname(file.originalname)}`);
  },
});
