import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';
import { passport } from '../../../auth/passport.setup';
import { LoginRequestBody } from './validator';

export const loginController = (
  req: Request<{}, LoginRequestBody, {}, {}>,
  res: Response,
  next: NextFunction
) => {
  passport.authenticate('local', function (err, user, info) {
    if (err) return next(err);

    !user
      ? res.status(401).json({ status: 'error', code: 'unauthorized' })
      : res.json({
          token: jwt.sign({ id: user.id }, process.env.PASSPORT_SECRET_KEY!),
        });
  })(req, res, next);
};
