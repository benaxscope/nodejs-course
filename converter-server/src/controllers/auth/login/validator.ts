import { NextFunction, Request, Response } from 'express';
import { z } from 'zod';

const loginRequestSchema = z.object({
  username: z.string(),
  password: z.string(),
});
export type LoginRequestBody = z.infer<typeof loginRequestSchema>;

export const loginValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const validationResult = loginRequestSchema.safeParse(req.body);

  if (!validationResult.success) {
    return res.status(401).json(validationResult.error);
  }

  return next();
};
