import { Request, Response } from 'express';
import { RegisterRequestBody } from './validator';
import { createUser } from '@/services/user/create-user';

export const registerController = async (
  req: Request<{}, {}, RegisterRequestBody, {}>,
  res: Response
) => {
  const user = await createUser({ ...req.body, provider: null });

  return res.json(user);
};
