import { NextFunction, Request, Response } from 'express';
import { z } from 'zod';

const registerRequestSchema = z.object({
  username: z.string(),
  password: z.string(),
});
export type RegisterRequestBody = z.infer<typeof registerRequestSchema>;

export const registerValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const validationResult = registerRequestSchema.safeParse(req.body);

  if (!validationResult.success) {
    return res.status(401).json(validationResult.error);
  }

  return next();
};
