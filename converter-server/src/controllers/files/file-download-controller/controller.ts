import { READY_FILES_PATH } from '@/services/files/config';
import { getFileInfo } from '@/services/files/get-file-info';
import { getClientId } from '@/utils/get-client-id';
import { Request, Response } from 'express';
import path from 'path';

export const fileDownloadController = (
  req: Request<{ id: string }, {}, {}, {}>,
  res: Response
) => {
  const clientId = getClientId(req);
  const fileId = req.params.id;

  try {
    const fileInfo = getFileInfo(clientId, fileId);
    if (fileInfo.status !== 'ready') {
      const filePath = path.resolve(READY_FILES_PATH, `${fileId}.webp`);

      res.download(filePath);
    } else {
      res.status(400).json({ error: 'File with given ID is not ready' });
    }
  } catch (e) {
    res.status(404).json({ error: 'File with given ID does not exists' });
  }
};
