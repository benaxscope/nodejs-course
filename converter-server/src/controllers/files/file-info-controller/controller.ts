import { getFileInfo } from '@/services/files/get-file-info';
import { getClientId } from '@/utils/get-client-id';
import { Request, Response } from 'express';

export const fileInfoController = (
  req: Request<{ id: string }, {}, {}, {}>,
  res: Response
) => {
  try {
    const clientId = getClientId(req);
    const fileId = req.params.id;
    const fileInfo = getFileInfo(clientId, fileId);

    res.json({
      data: fileInfo,
    });
  } catch (e) {
    res.status(404).json({ error: 'file with given ID not found' });
  }
};
