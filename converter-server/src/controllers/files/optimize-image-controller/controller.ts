import { Request, Response } from 'express';
import { optimizeImage } from '@/services/files/image-optimize';
import path from 'path';
import { getUnregisteredUser } from '@/services/user/get-unregistered-user';
import { decrementUploads } from '@/services/user/decrement-uploads';
import { isAuthorized } from '@/utils/is-authorized';

export const optimizeImageController = async (req: Request, res: Response) => {
  const isUserAuthorized = isAuthorized(req);
  const user = isUserAuthorized
    ? req.user!
    : await getUnregisteredUser(req.sessionID);

  if (user.availableUploads === 0) {
    return res.status(400).json({
      error: 'You reached the limit of available uploads',
    });
  }

  const fileId = path.parse(req.file!.filename).name;
  optimizeImage(user.id, fileId, req.file!.path);

  await decrementUploads(user.id, isUserAuthorized);

  return res.json({ data: { fileId } });
};
