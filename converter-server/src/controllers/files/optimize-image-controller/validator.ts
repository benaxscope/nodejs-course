import { NextFunction, Request, Response } from 'express';
import multer, { MulterError } from 'multer';
import { storageConfig } from '@/configs/multer';
import { IMAGE_MIMES } from '@/constants/mime-types';
import { getUnregisteredUser } from '@/services/user/get-unregistered-user';
import { decrementUploads } from '@/services/user/decrement-uploads';

const MAX_FILE_SIZE = 5e7;
const MAX_FILE_SIZE_ERROR = 'Error: max file size is 50mb';
const REQUIRED_FILE_ERROR = 'Error: file is required';
const FILE_TYPE_ERROR = 'Error: only images are allowed';

const optimizeImageUploader = multer({
  storage: storageConfig,
  limits: {
    fileSize: MAX_FILE_SIZE,
  },
  fileFilter: (req, file, callback) => {
    const mimetype = file.mimetype;
    if (!IMAGE_MIMES.includes(mimetype)) {
      callback(new Error(FILE_TYPE_ERROR));
    }

    callback(null, true);
  },
});

export const optimizeImageValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  optimizeImageUploader.single('image')(req, res, err => {
    let error: string | null = null;
    if (err instanceof MulterError) {
      error = MAX_FILE_SIZE_ERROR;
      return;
    }

    if (err instanceof Error) {
      error = MAX_FILE_SIZE_ERROR;
      return;
    }

    if (!req.file) {
      error = REQUIRED_FILE_ERROR;
    }

    if (err) {
      throw new Error('unexpected error');
    }

    if (error !== null) {
      res.status(400).json({ error });
    }

    next();
  });
};

const allowedUnregisterMimeTypes = ['image/jpg', 'image/jpeg'];

export const fileExtensionValidator = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const file = req.file!;
  let isAuthorized = typeof req.user !== 'undefined';

  if (!isAuthorized) {
    if (!allowedUnregisterMimeTypes.includes(file.mimetype)) {
      return res.status(400).json({
        error: 'Login to upload more file extensions',
      });
    }
  }

  return next();
};
