import { processPayment } from '@/services/user/process-payment';
import type { Request, Response } from 'express';

export const payController = async (req: Request, res: Response) => {
  const userId = req.user!.id;

  await processPayment(userId);

  return res.json({ success: true });
};
