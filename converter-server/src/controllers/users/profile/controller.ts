import type { Request, Response } from 'express';

export const profileController = async (req: Request, res: Response) => {
  return res.json(req.user!);
};
