import cron from 'node-cron';
import {
  unregisteredUsersJSONPath,
  usersJSONPath,
} from './services/user/config';
import fs from 'fs/promises';
import { UnregisteredUsersFile, UsersFile } from './services/user/types';
import { UNREGISTERED_MONTHLY_UPLOADS } from './services/user/constants';

const updateUnregisteredUsersLimits = async () => {
  const data = await fs.readFile(unregisteredUsersJSONPath, 'utf8');
  const unregisteredUsersFile: UnregisteredUsersFile = JSON.parse(data);

  for (const unregisteredUser of unregisteredUsersFile.users) {
    unregisteredUser.availableUploads += UNREGISTERED_MONTHLY_UPLOADS;
  }

  await fs.writeFile(
    unregisteredUsersJSONPath,
    JSON.stringify(unregisteredUsersFile)
  );
};

const updateRegisteredUsersLimits = async () => {
  const data = await fs.readFile(usersJSONPath, 'utf8');
  const usersFile: UsersFile = JSON.parse(data);

  for (const user of usersFile.users) {
    user.availableUploads += UNREGISTERED_MONTHLY_UPLOADS;
  }

  await fs.writeFile(usersJSONPath, JSON.stringify(usersFile));
};

cron.schedule('0 0 0 * * *', updateRegisteredUsersLimits);
cron.schedule('0 0 0 1 * *', updateUnregisteredUsersLimits);
