import fs from 'fs';
import { UPLOAD_FILE_PATH } from '@/configs/multer';
import path from 'path';
import { optimizeImageProcessor } from '@/services/files/processors/optimize-image';

export interface ClientFileProcessingTask {
  clientId: string;
  fileId: string;
  processor: () => Promise<unknown>;
}

export class ClientFileProcessingTasksPool {
  tasksQueue: Array<ClientFileProcessingTask>;
  parallelCount: number;
  activeTasks: Array<ClientFileProcessingTask> = [];

  constructor(
    tasksQueue: Array<ClientFileProcessingTask> = [],
    parallelCount: number
  ) {
    this.tasksQueue = tasksQueue;
    this.parallelCount = parallelCount;
    this.run();
  }

  addTask(task: ClientFileProcessingTask) {
    this.tasksQueue.push(task);
  }

  async startWorker() {
    while (true) {
      const readyToProcessTask = this.tasksQueue.filter(({ clientId }) => {
        const isClientTaskProcessing =
          this.activeTasks.findIndex(
            activeTask => activeTask.clientId === clientId
          ) !== -1;

        return !isClientTaskProcessing;
      });

      if (readyToProcessTask.length === 0) {
        await new Promise((resolve, reject) => setImmediate(resolve));
        continue;
      }

      const task = readyToProcessTask.shift()!;
      this.tasksQueue = this.tasksQueue.filter(queueTask => queueTask !== task);

      this.activeTasks.push(task);

      await task.processor();

      this.activeTasks = this.activeTasks.filter(
        activeTask => activeTask !== task
      );
    }
  }

  private run() {
    for (let i = 0; i < this.parallelCount; i++) {
      this.startWorker();
    }
  }
}

export const runFileProcessingTaskPool = () => {
  const clientFolders = fs.readdirSync(UPLOAD_FILE_PATH);
  const filesToProcess = clientFolders
    .map(clientId => {
      const clientFolderPath = path.resolve(UPLOAD_FILE_PATH, clientId);

      const filesPath = fs
        .readdirSync(clientFolderPath)
        .map(fileName => path.resolve(clientFolderPath, fileName));

      return filesPath.map(filePath => ({
        clientId,
        filePath,
        fileId: path.parse(filePath).name,
      }));
    })
    .flat();

  const recoveredTasks: Array<ClientFileProcessingTask> = filesToProcess.map(
    ({ clientId, fileId, filePath }) => ({
      clientId,
      fileId,
      processor: () => optimizeImageProcessor(clientId, filePath),
    })
  );

  const fileProcessingTaskPool = new ClientFileProcessingTasksPool(
    recoveredTasks,
    3
  );

  return fileProcessingTaskPool;
};
