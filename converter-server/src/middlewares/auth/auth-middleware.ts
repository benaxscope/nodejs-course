import { checkToken } from '@/services/auth/check-token';
import { NextFunction, Request, Response } from 'express';

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization;

  if (!token) {
    res.status(401).json({ error: 'Authorization header with token required' });
    return;
  }

  const isTokenValid = await checkToken(token);
  if (!isTokenValid) {
    res.status(401).send('Invalid token');
    return;
  }

  req.clientId = token;
  next();
};
