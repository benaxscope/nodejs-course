import jwt from 'jsonwebtoken';
import { loginController } from '@/controllers/auth/login/controller';
import { loginValidator } from '@/controllers/auth/login/validator';
import { registerController } from '@/controllers/auth/register/controller';
import { registerValidator } from '@/controllers/auth/register/validator';
import express from 'express';
import passport from 'passport';

const authRoutes = express.Router();

authRoutes.post('/login', loginValidator, loginController);
authRoutes.post('/register', registerValidator, registerController);
authRoutes.post(
  '/check',
  passport.authenticate('bearer', { session: false }),
  (req, res) => {
    return res.json(req.user);
  }
);

authRoutes.get(
  '/google',
  passport.authenticate('google', { scope: ['profile', 'email'] })
);
authRoutes.get(
  '/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/login',
    failureMessage: true,
  }),
  (req, res) => {
    res.json({
      token: jwt.sign({ id: req.user!.id }, process.env.PASSPORT_SECRET_KEY!),
    });
  }
);

export { authRoutes };
