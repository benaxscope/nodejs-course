import { fileDownloadController } from '@/controllers/files/file-download-controller';
import { fileInfoController } from '@/controllers/files/file-info-controller';
import {
  optimizeImageController,
  optimizeImageValidator,
} from '@/controllers/files/optimize-image-controller';
import { fileExtensionValidator } from '@/controllers/files/optimize-image-controller/validator';
import { authMiddleware } from '@/middlewares/auth/auth-middleware';
import { PassportUser } from '@/types/user';
import express from 'express';
import passport from 'passport';

const filesRoutes = express.Router();

filesRoutes.post(
  '/optimize',
  (req, res, next) => {
    passport.authenticate(
      'bearer',
      { session: false },
      (err: unknown, user: false | PassportUser) => {
        if (user) {
          req.user = user;
        }
        next();
      }
    )(req, res, next);
  },
  optimizeImageValidator,
  fileExtensionValidator,
  optimizeImageController
);
filesRoutes.get('/:id/download', authMiddleware, fileDownloadController);
filesRoutes.get('/:id', authMiddleware, fileInfoController);

export { filesRoutes };
