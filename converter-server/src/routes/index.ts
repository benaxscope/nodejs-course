import express from 'express';
import { healthController } from '../controllers/health/health.controller';
import { filesRoutes } from './files/files.routes';
import { authRoutes } from './auth/auth.routes';
import { usersRoutes } from './users/users.routes';

const routes = express.Router();

routes.get('/health', healthController);
routes.use('/files', filesRoutes);
routes.use('/auth', authRoutes);
routes.use('/users', usersRoutes);

export { routes };
