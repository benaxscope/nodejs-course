import { payController } from '@/controllers/users/pay/controller';
import { profileController } from '@/controllers/users/profile/controller';
import express from 'express';
import passport from 'passport';

const usersRoutes = express.Router();

usersRoutes.get(
  '/profile',
  passport.authenticate('bearer', { session: false }),
  profileController
);
usersRoutes.post(
  '/pay',
  passport.authenticate('bearer', { session: false }),
  payController
);

export { usersRoutes };
