import fs from 'fs/promises';
import { tokensJSONPath } from './config';
import { TokensFile } from './types';

export const checkToken = (token: string) =>
  fs.readFile(tokensJSONPath, 'utf8').then(data => {
    const tokensFile: TokensFile = JSON.parse(data);

    const tokens = tokensFile.tokens;

    return tokens.includes(token);
  });
