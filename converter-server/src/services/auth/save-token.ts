import fs from 'fs/promises';
import { tokensJSONPath } from './config';
import { TokensFile } from './types';

export const saveToken = (token: string) =>
  fs.readFile(tokensJSONPath, 'utf8').then(data => {
    const tokensFile: TokensFile = JSON.parse(data);

    tokensFile.tokens.push(token);

    return fs.writeFile(tokensJSONPath, JSON.stringify(tokensFile));
  });
