import { fileProcessingTaskPool } from '@/app';
import { FileInfo, FileProcessingTaskStatus } from './types';
import path from 'path';
import fs from 'fs';
import { READY_FILES_PATH } from './config';

export const getFileInfo = (clientId: string, fileId: string): FileInfo => {
  const isTaskInQueue =
    fileProcessingTaskPool.tasksQueue.findIndex(
      taskInQueue => taskInQueue.fileId === fileId
    ) !== -1;
  if (isTaskInQueue) {
    return {
      fileId,
      status: 'queue',
    };
  }

  const isTaskProcessing =
    fileProcessingTaskPool.activeTasks.findIndex(
      activeTask => activeTask.fileId === fileId
    ) !== -1;
  if (isTaskProcessing) {
    return {
      fileId,
      status: 'processing',
    };
  }

  const processedFileName = `${fileId}.webp`;
  const processedFilePath = path.resolve(
    READY_FILES_PATH,
    clientId,
    processedFileName
  );

  const isFileProcessed = fs.existsSync(processedFilePath);
  if (isFileProcessed) {
    return {
      fileId,
      status: 'processing',
    };
  }

  throw new Error('file with given fileId not found');
};
