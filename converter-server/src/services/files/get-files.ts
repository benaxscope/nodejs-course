import { fileProcessingTaskPool } from '@/app';
import { FileProcessingTaskStatus } from './types';
import { getReadyFiles } from './get-ready-files';

interface FileInfo {
  fileId: string;
  status: FileProcessingTaskStatus;
}

export const getFiles = (clientId: string): Array<FileInfo> => {
  const filesInQueue: Array<FileInfo> = fileProcessingTaskPool.tasksQueue
    .filter(task => task.clientId === clientId)
    .map(task => ({ fileId: task.fileId, status: 'queue' }));

  const processingFiles: Array<FileInfo> = fileProcessingTaskPool.activeTasks
    .filter(task => task.clientId === clientId)
    .map(task => ({ fileId: task.fileId, status: 'processing' }));

  const readyFiles = getReadyFiles(clientId);

  return [...filesInQueue, ...processingFiles, ...readyFiles];
};
