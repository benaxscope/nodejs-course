import path from 'path';
import fs from 'fs';
import { FileInfo } from './types';
import { READY_FILES_PATH } from './config';

export const getReadyFiles = (clientId: string) => {
  const clientReadyFilesPath = path.resolve(READY_FILES_PATH, clientId);
  const isClientReadyFilesDirExists = fs.existsSync(clientReadyFilesPath);
  if (isClientReadyFilesDirExists) {
    const fileIds = fs.readdirSync(clientReadyFilesPath).map(fileName => {
      const fileId = path.parse(fileName).name;

      return fileId;
    });

    const readyFiles: Array<FileInfo> = fileIds.map(fileId => ({
      fileId,
      status: 'ready',
    }));
    return readyFiles;
  }

  return [];
};
