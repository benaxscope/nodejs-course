import { fileProcessingTaskPool } from '@/app';
import { optimizeImageProcessor } from './processors/optimize-image';

export const optimizeImage = (
  clientId: string,
  fileId: string,
  filePath: string
) => {
  fileProcessingTaskPool.addTask({
    clientId,
    fileId,
    processor: () => optimizeImageProcessor(clientId, filePath),
  });
};
