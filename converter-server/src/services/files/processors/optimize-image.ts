import path from 'path';
import fs from 'fs';
import fsPromises from 'fs/promises';
import { READY_FILES_PATH } from '../config';
import sharp from 'sharp';

export const optimizeImageProcessor = async (
  clientId: string,
  filePath: string
) => {
  const clientReadyFilesDir = path.join(READY_FILES_PATH, clientId);
  const isClientReadyFilesDirExists = fs.existsSync(clientReadyFilesDir);
  if (!isClientReadyFilesDirExists) {
    fs.mkdirSync(clientReadyFilesDir);
  }

  const fileName = path.parse(filePath).name + '.webp';
  const outPath = path.resolve(clientReadyFilesDir, fileName);

  return sharp(filePath)
    .webp()
    .toFile(outPath)
    .then(() => fsPromises.unlink(filePath));
};
