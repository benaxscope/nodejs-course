export type FileProcessingTaskStatus = 'queue' | 'processing' | 'ready';
export interface FileInfo {
  fileId: string;
  status: FileProcessingTaskStatus;
}
