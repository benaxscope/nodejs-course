import path from 'path';

export const usersJSONPath = path.resolve(__dirname, 'users.json');
export const unregisteredUsersJSONPath = path.resolve(
  __dirname,
  'unregistered-users.json'
);
