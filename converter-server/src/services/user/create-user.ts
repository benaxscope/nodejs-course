import fs from 'fs/promises';
import { usersJSONPath } from './config';
import { PassportUser } from '../../types/user';
import { UsersFile } from './types';
import { randomUUID } from 'crypto';
import { REGISTERED_DAILY_UPLOADS } from './constants';

export const createUser = (
  user: Omit<PassportUser, 'id' | 'availableUploads'>
) =>
  fs.readFile(usersJSONPath, 'utf8').then(async data => {
    const usersFile: UsersFile = JSON.parse(data);

    const userId = randomUUID();
    const newUser: PassportUser = {
      ...user,
      id: userId,
      availableUploads: REGISTERED_DAILY_UPLOADS,
    };

    usersFile.users.push(newUser);

    await fs.writeFile(usersJSONPath, JSON.stringify(usersFile));

    return newUser;
  });
