import fs from 'fs/promises';
import { unregisteredUsersJSONPath, usersJSONPath } from './config';
import { UnregisteredUsersFile, UsersFile } from './types';

export const decrementUploads = async (id: string, isRegistered: boolean) => {
  const filePath = isRegistered ? usersJSONPath : unregisteredUsersJSONPath;
  const data = await fs.readFile(filePath, 'utf8');
  const usersFile: UsersFile | UnregisteredUsersFile = JSON.parse(data);

  const userIdx = usersFile.users.findIndex(user => user.id === id);
  if (userIdx === -1) {
    throw new Error('User with given sessionId does not exist');
  }

  usersFile.users[userIdx].availableUploads -= 1;

  await fs.writeFile(filePath, JSON.stringify(usersFile));

  return true;
};
