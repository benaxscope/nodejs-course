import fs from 'fs/promises';
import { usersJSONPath } from './config';
import { UsersFile } from './types';

export const findUserByCredentials = ({
  username,
  password,
}: {
  username: string;
  password: string;
}) =>
  fs.readFile(usersJSONPath, 'utf8').then(data => {
    const usersFile: UsersFile = JSON.parse(data);

    const user =
      usersFile.users.find(
        user => user.username === username && user.password === password
      ) ?? null;
    if (user === null) {
      throw new Error('User with given id does not exist');
    }

    return user;
  });
