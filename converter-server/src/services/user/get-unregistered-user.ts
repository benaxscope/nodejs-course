import { UnregesteredUser } from '@/types/user';
import fs from 'fs/promises';
import { unregisteredUsersJSONPath } from './config';
import { UNREGISTERED_MONTHLY_UPLOADS } from './constants';
import { UnregisteredUsersFile, UsersFile } from './types';

export const getUnregisteredUser = async (id: string) => {
  const data = await fs.readFile(unregisteredUsersJSONPath, 'utf8');
  const unregisteredUsersFile: UnregisteredUsersFile = JSON.parse(data);

  let user: UnregesteredUser | null =
    unregisteredUsersFile.users.find(user => user.id === id) ?? null;

  if (user === null) {
    user = {
      id: id,
      availableUploads: UNREGISTERED_MONTHLY_UPLOADS,
    };

    unregisteredUsersFile.users.push(user);

    await fs.writeFile(
      unregisteredUsersJSONPath,
      JSON.stringify(unregisteredUsersFile)
    );
  }

  return user;
};
