import { PassportUser, ProviderOptions } from '@/types/user';
import fs from 'fs/promises';
import { usersJSONPath } from './config';
import { UsersFile } from './types';

type GetUserParams = { id: string } | { provider: ProviderOptions };

export const getUser = (params: GetUserParams) =>
  fs.readFile(usersJSONPath, 'utf8').then(data => {
    const usersFile: UsersFile = JSON.parse(data);

    let user: PassportUser | null;

    if ('id' in params) {
      user = usersFile.users.find(user => user.id === params.id) ?? null;
    } else {
      user =
        usersFile.users.find(
          user =>
            user.provider &&
            user.provider.name === params.provider.name &&
            user.provider.id === params.provider.id
        ) ?? null;
    }

    if (user === null) {
      throw new Error('User with given id does not exist');
    }

    return user;
  });
