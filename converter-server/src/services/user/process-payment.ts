import { PassportUser } from '@/types/user';
import fs from 'fs/promises';
import { usersJSONPath } from './config';
import { PAYMENT_ADDITIONAL_UPLOADS } from './constants';
import { UsersFile } from './types';

export const processPayment = (id: string) =>
  fs.readFile(usersJSONPath, 'utf8').then(async data => {
    const usersFile: UsersFile = JSON.parse(data);

    const userIdx = usersFile.users.findIndex(user => id === user.id);
    if (userIdx === -1) {
      throw new Error('User with given id does not exist');
    }

    usersFile.users[userIdx].availableUploads += PAYMENT_ADDITIONAL_UPLOADS;

    await fs.writeFile(usersJSONPath, JSON.stringify(usersFile));

    return true;
  });
