import { PassportUser, UnregesteredUser } from '@/types/user';

export interface UsersFile {
  users: PassportUser[];
}

export interface UnregisteredUsersFile {
  users: UnregesteredUser[];
}
