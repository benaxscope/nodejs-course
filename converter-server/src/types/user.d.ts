interface ProviderOptions {
  name: string;
  id: string;
}
export interface PassportUser {
  id: string;
  username: string;
  password: string;
  availableUploads: number;
  provider: ProviderOptions | null;
}

export interface UnregesteredUser {
  id: string;
  availableUploads: number;
}
