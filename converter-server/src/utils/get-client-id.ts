import { Request } from 'express';

export const getClientId = (req: Request) => {
  return req.user?.id ?? req.sessionID;
};
