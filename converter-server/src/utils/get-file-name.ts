export const getFileName = (filename: string) =>
  filename.split('.').splice(-1).join('.');
