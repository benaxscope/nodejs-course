import { Request } from 'express';

export const isAuthorized = (req: Request) => {
  return typeof req.user !== 'undefined';
};
