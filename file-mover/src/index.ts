import { Command } from 'commander';
import { resolve } from 'path';
import { existsSync } from 'fs';
import { processFolder } from './processFolder';

const { inPath, outPath, config } = new Command()
  .requiredOption('--inPath <char>')
  .requiredOption('--outPath <char>')
  .parse()
  .opts<{ inPath: string; outPath: string; config: string }>();

const inDir = resolve(inPath);
const outDir = resolve(outPath);

async function main () {
  if (!existsSync(inDir)) {
    throw new Error('no input directory path')
  }
  if (!existsSync(outDir)) {
    throw new Error('no input directory path')
  }

  await processFolder(inDir, outDir)
}

main()