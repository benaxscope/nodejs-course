import { processImage } from './processImage';
import { processSimpleFile } from './processSimpleFile';

const imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];

export function isImage(fileName: string): boolean {
  const extension = fileName.split('.').pop()!;
  return imageExtensions.includes(extension);
}

export function processFile(filePath: string, outDir: string) {
  return isImage(filePath)
    ? processImage(filePath, outDir)
    : processSimpleFile(filePath, outDir)
}

