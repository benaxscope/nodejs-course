import fs from 'node:fs';
import { processFile } from './processFile';
import path from 'node:path';

export async function processFolder(
  directory: string,
  outPath: string
): Promise<void> {
  const files = fs.readdirSync(directory, {
    withFileTypes: true,
  });
  for (const file of files) {
    const filePath = path.join(directory, file.name);
    
    if (file.isFile()) {
      await processFile(filePath, outPath);
    } else if (file.isDirectory()) {
      await processFolder(filePath, outPath);
    }
  }
}
