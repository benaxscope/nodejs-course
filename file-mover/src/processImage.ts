import md5 from 'md5';
import sharp from 'sharp';
import { existsSync } from 'fs';
import path from 'node:path';

const getOutImageName = (filePath: string) => {
  const fileNameHash = md5(filePath);
  return `${fileNameHash}.webp`;
};

export async function processImage(filePath: string, outDir: string) {
  const outImageName = getOutImageName(filePath);
  const outPath = path.join(outDir, outImageName);

  const fileExists = existsSync(outPath);
  if (fileExists) {
    return;
  }

  console.log('processImage');
  await sharp(filePath).webp().toFile(outPath);
}
