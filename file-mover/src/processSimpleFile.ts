import { createReadStream, createWriteStream, existsSync } from 'node:fs';
import md5 from 'md5';
import { getFileExtension } from './utils/getFIleExtension';
const { pipeline } = require('node:stream/promises');

const getOutSimpleFileName = (filePath: string) => {
  const fileExtenstion = getFileExtension(filePath);
  const fileNameHash = md5(filePath);
  return `${fileNameHash}.${fileExtenstion}`;
};

export async function processSimpleFile(filePath: string, outDir: string) {
  const outFileName = getOutSimpleFileName(filePath);
  const outPath = `${outDir}/${outFileName}`;

  const fileExists = existsSync(outPath);
  if (fileExists) {
    return;
  }

  console.log('processFile')
  await pipeline(createReadStream(filePath), createWriteStream(outPath));
}
